# Imagen Base
FROM node:latest

# Dierctorio de la app
WORKDIR /app

# Copia de Archivos
#ADD . /app
ADD /build/default /app/build/default
ADD server.js /app
ADD package.json /app

# Dependencias
RUN npm install

# Puerto que expongo
EXPOSE 3000

# Comando
CMD ["npm", "start"]
